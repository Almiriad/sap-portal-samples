package com.almiriad.sap.portal.user;

import java.io.IOException;
import java.util.Base64;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sap.security.api.IUser;
import com.sap.security.api.IUserFactory;
import com.sap.security.api.IUserMaint;
import com.sap.security.api.UMException;
import com.sap.security.api.UMFactory;
import com.sapportals.portal.prt.component.IPortalComponentRequest;
import com.sapportals.portal.prt.logger.ILogger;
import com.sapportals.portal.prt.runtime.PortalRuntime;

public class UserInfoSampleServlet extends HttpServlet {
	private static final long serialVersionUID = 1597407343086283965L;
	private static final ILogger logger = PortalRuntime.getLogger();
	private IPortalComponentRequest portalRequest;
	private IPortalComponentRequest request;
	private JsonObject jsonUser = null;
	private JsonObjectBuilder objectBuilder;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Object obj = req.getAttribute("com.sapportals.portal.prt.component.IPortalComponentRequest");

		resp.setContentType("application/json");
		resp.setCharacterEncoding("UTF-8");
		objectBuilder = Json.createObjectBuilder();
		try {
			if (obj instanceof IPortalComponentRequest) {
				request = (IPortalComponentRequest) obj;

				// Process several users
//				String paramUsrs = request.getParameter("users");
//				if (paramUsrs != null && !paramUsrs.equals("")) {
//					String[] splitUsrs = paramUsrs.split(";");
//
//					JsonArrayBuilder arrayUsers = Json.createArrayBuilder();
//					for (String userID : splitUsrs) {
//						IUser user = UMFactory.getUserFactory().getUserByLogonID(userID);
//						arrayUsers.add(getUserPhoto(user));
//					}
//					objectBuilder.add("users", arrayUsers);
//				} else {
					IUser user = request.getUser();
//					String paramUsr = request.getParameter("user");
//					if (paramUsr != null && !paramUsr.equals("")) {
//						user = UMFactory.getUserFactory().getUserByLogonID(paramUsr);
//					}
					JsonObjectBuilder userInfo = Json.createObjectBuilder();
					userInfo.add("user-name", user.getDisplayName());
					userInfo.add("user-id", user.getUniqueName());
					userInfo.add("user-email", user.getEmail());
					objectBuilder.add("user", userInfo);
//				}
				jsonUser = objectBuilder.build();
				resp.setStatus(HttpServletResponse.SC_OK);
				resp.getWriter().write(jsonUser.toString());
			} else {
				objectBuilder = Json.createObjectBuilder();
				objectBuilder.add("error", "Not a portal request");
				jsonUser = objectBuilder.build();
				resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				resp.getWriter().write(jsonUser.toString());
			}
		} catch (Exception e) {
			objectBuilder = Json.createObjectBuilder();
			objectBuilder.add("error", "Echec");
			jsonUser = objectBuilder.build();
			resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			logger.severe(e, "UserInfoSampleServlet::doGet");
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Object obj = req.getAttribute("com.sapportals.portal.prt.component.IPortalComponentRequest");
		if (obj instanceof IPortalComponentRequest) {
			request = (IPortalComponentRequest) obj;
		}
		resp.setContentType("application/json");
		resp.setCharacterEncoding("UTF-8");
		objectBuilder = Json.createObjectBuilder();
		try {

			// Process several users
			String paramUsrs = request.getParameter("users");
			if (paramUsrs != null && !paramUsrs.equals("")) {
				String[] splitUsrs = paramUsrs.split(";");

				JsonArrayBuilder arrayUsers = Json.createArrayBuilder();
				for (String userID : splitUsrs) {
					IUser user = UMFactory.getUserFactory().getUserByLogonID(userID);
					arrayUsers.add(getUserPhoto(user));
				}
				objectBuilder.add("users", arrayUsers);
			} else {
				IUser user = request.getUser();
				String paramUsr = request.getParameter("user");
				if (paramUsr != null && !paramUsr.equals("")) {
					user = UMFactory.getUserFactory().getUserByLogonID(paramUsr);
				}
				objectBuilder.add("user", getUserPhoto(user));
			}
			jsonUser = objectBuilder.build();
			resp.setStatus(HttpServletResponse.SC_OK);
			resp.getWriter().write(jsonUser.toString());

		} catch (

		Exception e)

		{
			objectBuilder = Json.createObjectBuilder();
			objectBuilder.add("error", "Echec");
			jsonUser = objectBuilder.build();
			resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			logger.severe(e, "UserInfoSampleServlet::doPost");
		}

	}

	private JsonObjectBuilder getUserPhoto(IUser user) {
		JsonObjectBuilder objUser = Json.createObjectBuilder();
		JsonArrayBuilder arrayAttrBuilder = Json.createArrayBuilder();
		byte[] usrPhoto = user.getBinaryAttribute("com.sap.security.core.usermanagement", "photo");
		if (usrPhoto != null) {
			StringBuilder sb = new StringBuilder();
			sb.append("data:image/png;base64,");
			sb.append(new String(Base64.getEncoder().encode(usrPhoto)));
			objUser.add("user-photo", sb.toString());
		} else {
			objUser.add("user-photo", "");
		}
		objUser.add("user-id", user.getUniqueName());
		return objUser;
	}

	private void getUserPhoto(IUser user, String base64) {
		byte[] usrPhoto = Base64.getDecoder().decode(base64);

		try {
			IUserFactory userFact = UMFactory.getUserFactory();
			IUserMaint modUser = userFact.getMutableUser(user.getUniqueID());
			modUser.setBinaryAttribute("com.sap.security.core.usermanagement", "photo", usrPhoto);
		} catch (UMException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
